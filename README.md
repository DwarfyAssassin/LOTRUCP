Welcome to the LOTR unofficial compatibility patch (LOTR-UCP) repository. LOTRUCP is a minecraft forge (core) mod for LOTRMod by Mevans. It has as goal to fix as many compatibility issues with LOTR as possible.

__Instalation__

You can download the mod from gitlab in the [release tab](https://gitlab.com/DwarfyAssassin/LOTRUCP/-/releases "Go to release tab").

LOTR-UCP can be installed like any other forge mod, just put the mod in your mods folder and it will do it's thing. For it to have any effect you need [LOTR Mod](https://lotrminecraftmod.fandom.com/wiki/The_Lord_of_the_Rings_Minecraft_Mod_Wiki "LOTR Mod wiki page.") by Mevans and one of the mod for which it has a patch (listed below).

This mod is a forge coremod which runs on minecraft 1.7.10.


__Bug & Suggestions__

Any suggestions and or bug reports are welcome in the issues tab.

__Current Patches__
- General
  - Bandit and magpie steal modded item crash.
  - Armor stand modded armor render crash.
  - Lot's of stack traces clogging the logs caused by fake players doing certain actions in banner protected area's.
- Botania
  - Various crafting recipes
  - Kekimurus not eating cakes.
  - Authum rune crafting recipe crash in the Lexica Botania
- DragonAPI
  - Crash on startup
- Screenshots Enhanced
  - Oddment Collector render crash.
- Thaumcraft
  - Coin counter crash when opening the golem GUI.
  - Golems not being able to do certain actions in banner protected area's.
- Pam's Harvestcraft
  - Bees with durability modifiers crash the game.
- Twilight Forest
  - Crash on startup
  
__Other Patcher__
Here you can find links for other patches made by other members in the comunity.

More player Models on [curse forge](https://www.curseforge.com/minecraft/mc-mods/mpm-lotr/files/ "Go to MPMLOTR on curse forge.") by Malvegil.