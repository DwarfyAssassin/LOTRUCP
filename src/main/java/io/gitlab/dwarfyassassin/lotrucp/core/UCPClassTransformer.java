package io.gitlab.dwarfyassassin.lotrucp.core;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import cpw.mods.fml.relauncher.FMLLaunchHandler;
import cpw.mods.fml.relauncher.ReflectionHelper;
import io.gitlab.dwarfyassassin.lotrucp.core.hooks.PreMCHooks;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.base.Patcher;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.LaunchClassLoader;

public class UCPClassTransformer implements IClassTransformer {

    static {
        FMLLaunchHandler launchHandler = ReflectionHelper.getPrivateValue(FMLLaunchHandler.class, null, "INSTANCE");
        LaunchClassLoader classLoader = ReflectionHelper.getPrivateValue(FMLLaunchHandler.class, launchHandler, "classLoader");
        
        PreMCHooks.transformerExclusionsTweaks(classLoader);
    }

    @Override
    public byte[] transform(String name, String transformedName, byte[] classBytes) {
        boolean ran = false;

        for(Patcher patcher : UCPCoreMod.activePatches) {
            if(patcher.canRun(name)) {
                ran = true;

                ClassNode classNode = new ClassNode();
                ClassReader classReader = new ClassReader(classBytes);
                classReader.accept(classNode, 0);

                UCPCoreMod.log.info("Running patcher " + patcher.getName() + " for " + name);
                patcher.run(name, classNode);

                ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                classNode.accept(writer);
                classBytes = writer.toByteArray();
            }
        }

        if(ran) {
            UCPCoreMod.activePatches.removeIf(patcher -> patcher.isDone());
            if(UCPCoreMod.activePatches.isEmpty()) UCPCoreMod.log.info("Ran all active patches.");
        }

        return classBytes;
    }
}
