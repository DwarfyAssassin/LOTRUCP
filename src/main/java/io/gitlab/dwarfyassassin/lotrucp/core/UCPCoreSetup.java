package io.gitlab.dwarfyassassin.lotrucp.core;

import java.util.Map;
import org.apache.logging.log4j.LogManager;
import cpw.mods.fml.relauncher.IFMLCallHook;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.BotaniaPatcher;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.FMLPatcher;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.LOTRPatcher;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.ScreenshotEnhancedPatcher;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.ThaumcraftPatcher;

public class UCPCoreSetup implements IFMLCallHook {

    @Override
    public Void call() throws Exception {
        UCPCoreMod.log = LogManager.getLogger("LOTR-UCP");

        UCPCoreMod.registerPatcher(new FMLPatcher());
        UCPCoreMod.registerPatcher(new BotaniaPatcher());
        UCPCoreMod.registerPatcher(new ScreenshotEnhancedPatcher());
        UCPCoreMod.registerPatcher(new ThaumcraftPatcher());
        UCPCoreMod.registerPatcher(new LOTRPatcher());

        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
    }
}
